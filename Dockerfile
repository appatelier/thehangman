FROM microsoft/dotnet

RUN apt-get update
RUN wget -qO- https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y build-essential nodejs

COPY . /app

WORKDIR /app

ENV ASPNETCORE_URLS http://+:5000
ENV ASPNETCORE_ENVIRONMENT docker

RUN ["dotnet", "restore"]
RUN ["dotnet", "build"]

EXPOSE 5000/tcp

CMD ["dotnet", "run", "--server.urls", "http://*:5000"]
