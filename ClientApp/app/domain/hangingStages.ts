export enum HangingStages {
  None,
  Head,
  Neck,
  Corpus,
  RightArm,
  LeftArm,
  RightHand,
  LeftHand,
  RightLeg,
  LeftLeg,
  RightFoot,
  LeftFoot
}
