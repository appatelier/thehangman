export enum GameStatus {
  InProgress,
  Success,
  Failure
}
