export class WordWrapper {
  id?: number;
  word: string;
  visible?: boolean;
  empty?: boolean;
}
