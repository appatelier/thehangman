import { Injectable, Inject } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Http } from '@angular/http';
import { WordWrapper } from '../domain/wordWrapper';

@Injectable()
export class WordService {
  private static readonly wordsApiUrl: string = 'http://api.wordnik.com:80/v4/words.json/randomWord';
  private static readonly wordsApiKey: string = 'a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5';
  private readonly minWordLength: number = 6;
  private readonly maxWordLength: number = 11;
  private wordSource = new Subject<string>();
  private wordWrapper: WordWrapper;
  word$ = this.wordSource.asObservable();

  constructor(private http: Http) {
  }

  fetchWord() {
    let options =  { params: new WordQuery(this.minWordLength, this.maxWordLength, WordService.wordsApiKey) };

    this.http.get(WordService.wordsApiUrl, options).subscribe(result => {
      this.wordWrapper = result.json() as WordWrapper;
      this.wordSource.next(this.wordWrapper.word);
    });
  }
}

class WordQuery {
  constructor(public minLength: number, public maxLength: number, public api_key: string) {
  }
}
