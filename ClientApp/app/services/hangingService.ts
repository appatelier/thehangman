import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { HangingStages } from '../domain/hangingStages';

@Injectable()
export class HangingService {
  private stagesSource = new Subject<HangingStages>();
  private stage: HangingStages;
  stages$ = this.stagesSource.asObservable();

  constructor() {
    this.resetStages();
  }

  goToNextStage() {
    if (this.stage == HangingStages.LeftFoot) { return; };
    this.stage++;
    this.stagesSource.next(this.stage);
  }

  resetStages() {
    this.stage = HangingStages.None;
    this.stagesSource.next(this.stage);    
  }
}
