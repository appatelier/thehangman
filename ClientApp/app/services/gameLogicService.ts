import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { HangingStages } from '../domain/hangingStages';
import { HangingService } from './hangingService';
import { GameStatus } from '../domain/gameStatus';
import { WordService } from './wordService';

@Injectable()
export class GameLogicService {
  private word: string;
  private guessedLetters: string[];
  private guessedWordSource = new Subject<string>();
  private missedLetters: string[];
  private missedLettersSource = new Subject<string[]>();
  private gameStatus: GameStatus;
  private gameStatusSource = new Subject<GameStatus>();

  guessedWord$ = this.guessedWordSource.asObservable();
  missedLetters$ = this.missedLettersSource.asObservable();
  gameStatus$ = this.gameStatusSource.asObservable();
  
  constructor(private hangingService: HangingService, private wordService: WordService) {
    this.startGame();

    this.hangingService.stages$.subscribe(stage => {
      if (stage >= HangingStages.LeftFoot) {
        this.gameStatus = GameStatus.Failure;
        this.gameStatusSource.next(this.gameStatus);
      }
    });

    this.wordService.word$.subscribe(word => {
      this.word = word.toLowerCase();
      this.initGuessedWord();
      this.guessedWordSource.next(this.guessedWord);
      this.missedLettersSource.next(this.missedLetters);
      this.gameStatusSource.next(this.gameStatus);
    });
  }

  startGame() {
    if(this.gameStatus === GameStatus.InProgress) { return; }

    this.gameStatus = GameStatus.InProgress;
    this.missedLetters = [];
    this.guessedLetters = [];

    this.hangingService.resetStages();
    this.wordService.fetchWord();
  }

  checkLetter(letter: string) {
    if(this.gameStatus != GameStatus.InProgress) { return; };

    let processedLetter = letter.toLowerCase();   
 
    if (_.includes(this.word, processedLetter)) {
      this.addToGuessedLetters(processedLetter);

      if (this.guessedWord === this.word) {
        this.gameStatus = GameStatus.Success;
        this.gameStatusSource.next(this.gameStatus);
      }
    } else {
      this.addToMissedLetters(processedLetter);
    }
  }

  private get guessedWord(): string {
    return this.word
      .split('')
      .map(c => _.includes(this.guessedLetters, c) ? c : '_')
      .join('');    
  }

  private initGuessedWord() {
    const unveildedLettersCount = 2;
    if (this.word.length <= unveildedLettersCount) { return; };

    let unique = _(this.word).split('').uniq().join('');
    let getRandomLetter = (str: string) => str.charAt(Math.floor(Math.random() * str.length));

    for(let i = 0; i < unveildedLettersCount; i++) {
      let letter = getRandomLetter(unique);
      this.addToGuessedLetters(letter);
      unique = unique.replace(letter, '');
    }

    this.guessedWordSource.next(this.guessedWord);
  }

  private addToGuessedLetters(letter: string) {
    if (_.includes(this.guessedLetters, letter)) { return; }
    this.guessedLetters.push(letter);
    this.guessedWordSource.next(this.guessedWord);
  }

  private addToMissedLetters(letter: string) {
    if (_.includes(this.missedLetters, letter)) { return; }
    this.missedLetters.push(letter);
    this.missedLettersSource.next(this.missedLetters);
    this.hangingService.goToNextStage();    
  }
}
