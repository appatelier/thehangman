import { Component } from '@angular/core';
import { HangingStages } from '../../domain/hangingStages';
import { HangingService } from '../../services/hangingService';

@Component({
  selector: 'hangman',
  templateUrl: './hangman.component.html.hamlc',
  styleUrls: ['./hangman.component.scss']
})
export class HangmanComponent {
  stages = HangingStages;
  stage: HangingStages;

  constructor(private hangingService: HangingService) {
    this.hangingService.stages$.subscribe(stage => this.stage = stage);
  }
}
