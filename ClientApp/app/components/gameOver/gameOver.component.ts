import * as _ from 'lodash';
import { Component, HostListener } from '@angular/core';
import { HangmanComponent } from '../hangman/hangman.component';
import { HangingService } from '../../services/hangingService';
import { GameLogicService } from '../../services/gameLogicService';
import { GameStatus } from '../../domain/gameStatus';

@Component({
  selector: 'game-over',
  templateUrl: './gameOver.component.html.hamlc',
  styleUrls: ['./gameOver.component.scss'],
})
export class GameOverComponent {
  readonly actionButtonText: string = 'new word';
  gameOverText: string;
  show: boolean = false;

  constructor(private gameLogicService: GameLogicService) {
    this.gameLogicService.gameStatus$.subscribe(status => {
      const failureText = 'game over';
      const winText = 'you have won!';

      if (status === GameStatus.Failure) {
        this.gameOverText = failureText;
        this.show = true;
      } else if (status === GameStatus.Success) {
        this.gameOverText = winText;
        this.show = true;
      } else {
        this.show = false;
      }
    });
  }

  createNewGame() {
    this.gameLogicService.startGame();
  }
}
