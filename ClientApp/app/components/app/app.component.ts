import { Component } from '@angular/core';

@Component({
  selector: 'app',
  templateUrl: './app.component.html.hamlc',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
