import * as _ from 'lodash';
import { Component, HostListener } from '@angular/core';
import { HangmanComponent } from '../hangman/hangman.component';
import { HangingService } from '../../services/hangingService';
import { GameLogicService } from '../../services/gameLogicService';
import { WordService } from '../../services/wordService';

@Component({
  selector: 'home',
  templateUrl: './home.component.html.hamlc',
  styleUrls: ['./home.component.scss'],
  providers: [HangingService, GameLogicService, WordService]
})
export class HomeComponent {
  missedLetters: string[] = [];
  guessedLetters: string[] = [];
  
  constructor(private hangingService: HangingService, private gameLogicService: GameLogicService) {
    this.gameLogicService.guessedWord$.subscribe(word => {
      this.guessedLetters = _.padStart(word, 11, ' ').split('').map(c => c.trim());
    });

    this.gameLogicService.missedLetters$.subscribe(missedLetters => {
      this.missedLetters = missedLetters;
    });
  }

  range(range: number): number[] {
    return Array(range).fill(0);
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: any) {
    if (event.keyCode >= KeyRange.KeyRangeStart && event.keyCode <= KeyRange.KeyRangeEnd) {
      this.gameLogicService.checkLetter(event.key);
    }
  }

  getClass(letter: string) {
    return letter;
  }
}

export enum KeyRange {
  KeyRangeStart = 65,
  KeyRangeEnd = 90
}
