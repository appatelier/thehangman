import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { HangmanComponent } from './components/hangman/hangman.component';
import { GameOverComponent } from './components/gameOver/gameOver.component';

export const sharedConfig: NgModule = {
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    HangmanComponent,
    GameOverComponent
  ],
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: '**', redirectTo: 'home' }
    ])
  ]
};
